package cat.vazquez.joan.tetris_joan.framework.impl;

import android.graphics.Bitmap;

import cat.vazquez.joan.tetris_joan.framework.Graphics.PixmapFormat;
import cat.vazquez.joan.tetris_joan.framework.Pixmap;

public class AndroidPixmap implements Pixmap {
    Bitmap bitmap;
    PixmapFormat format;
    
    public AndroidPixmap(Bitmap bitmap, PixmapFormat format) {
        this.bitmap = bitmap;
        this.format = format;
    }

    public int getWidth() {
        return bitmap.getWidth();
    }

    public int getHeight() {
        return bitmap.getHeight();
    }

    public PixmapFormat getFormat() {
        return format;
    }

    public void dispose() {
        bitmap.recycle();
    }      
}
