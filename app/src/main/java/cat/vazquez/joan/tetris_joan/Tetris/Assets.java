package cat.vazquez.joan.tetris_joan.Tetris;

import cat.vazquez.joan.tetris_joan.framework.Music;
import cat.vazquez.joan.tetris_joan.framework.Pixmap;
import cat.vazquez.joan.tetris_joan.framework.Sound;

public class Assets {
    public static Pixmap background;
    public static Pixmap credits;
    public static Pixmap logo;
    public static Pixmap mainMenu;
    public static Pixmap buttons;
    public static Pixmap numbers;
    public static Pixmap ready;
    public static Pixmap pause;
    public static Pixmap gameOver;
    public static Pixmap piece_1;
    public static Pixmap piece_2;
    public static Pixmap piece_3;
    public static Pixmap piece_4;
    public static Pixmap piece_5;
    public static Pixmap piece_6;
    public static Pixmap piece_7;



    public static Sound click;
    public static Sound eat;
    public static Sound xoc;
    public static Sound HardDrop;
    public static Sound PieceMoveLR;
    public static Sound PieceRotateLR;
    public static Sound SoftDrop;
    public static Sound LineClear;
    public static Sound GameOver;
    public static Sound PLD;
    public static Sound Land;
    public static Music Theme;
}
