package cat.vazquez.joan.tetris_joan.framework;

public interface Sound {
    public void play(float volume);

    public void dispose();
}
