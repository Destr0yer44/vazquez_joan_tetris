package cat.vazquez.joan.tetris_joan.Tetris;

/**
 * Created by damviod on 11/12/15.
 */
public class Piece {

    boolean[][]shape;
    public int x, y;
    public int type;

    public Piece(int x, int y, int type) {
        this.x = x;
        this.y = y;
        this.type = type;
        CreateShape();
    }

    public Piece(Piece other)
    {
        this.x = other.x;
        this.y = other.y;
        this.type = other.type;
        CreateShape();
    }
//Aceder primer valor shape.lenght columna
// Aceder segundo shape[0].lenght; fila

    void CreateShape(){
        switch (type){

            case 1 : //Cuadrado ::
                shape = new boolean[2][2];
                shape[0][0] = true;
                shape[0][1] = true;
                shape[1][0] = true;
                shape[1][1] = true;
                break;

            case 2: //Z
                shape = new boolean[2][3];
                shape[0][1] = true;
                shape[0][2] = true;
                shape[1][0] = true;
                shape[1][1] = true;
                break;

            case 3: // Z2
                shape = new boolean[2][3];
                shape[0][0] = true;
                shape[0][1] = true;
                shape[1][1] = true;
                shape[1][2] = true;
                break;

            case 4: // 1/3
                shape = new boolean[2][3];
                shape[0][1] = true;
                shape[1][0] = true;
                shape[1][1] = true;
                shape[1][2] = true;
                break;

            case 5: // ..:
                shape = new boolean[2][3];
                shape[0][2] = true;
                shape[1][0] = true;
                shape[1][1] = true;
                shape[1][2] = true;
                break;

            case 6: // :..
                shape = new boolean[2][3];
                shape[0][0] = true;
                shape[1][0] = true;
                shape[1][1] = true;
                shape[1][2] = true;
                break;

            case 7: // ....
                shape = new boolean[4][1];
                shape[0][0] = true;
                shape[1][0] = true;
                shape[2][0] = true;
                shape[3][0] = true;
                break;
        }


    }
}
