package cat.vazquez.joan.tetris_joan.Tetris;

import java.util.List;

import cat.vazquez.joan.tetris_joan.framework.Game;
import cat.vazquez.joan.tetris_joan.framework.Graphics;
import cat.vazquez.joan.tetris_joan.framework.Input;
import cat.vazquez.joan.tetris_joan.framework.Screen;


public class CreditsScreen extends Screen{
    public CreditsScreen(Game game) {
        super(game);
    }

    @Override
    public void update(float deltaTime){
        List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        int len = touchEvents.size();
        for (int i = 0; i < len; i++) {
            Input.TouchEvent event = touchEvents.get(i);
            if (event.type == Input.TouchEvent.TOUCH_UP) {
                if (event.x < 64 && event.y > 416) {
                    Assets.click.play(1);
                    game.setScreen(new MainMenuScreen(game));
                    return;
                }
            }
        }
    }

    @Override
    public void render(float deltaTime) {

        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.credits, 0, 0);
        g.drawPixmap(Assets.buttons, 0, 416, 64, 64, 64, 64);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
