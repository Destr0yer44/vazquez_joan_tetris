package cat.vazquez.joan.tetris_joan.Tetris;

import cat.vazquez.joan.tetris_joan.framework.Game;
import cat.vazquez.joan.tetris_joan.framework.Graphics;
import cat.vazquez.joan.tetris_joan.framework.Graphics.PixmapFormat;
import cat.vazquez.joan.tetris_joan.framework.Screen;

public class LoadingScreen extends Screen {
    public LoadingScreen(Game game) {
        super(game);
    }

    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        Assets.background = g.newPixmap("background.png", PixmapFormat.RGB565);
        Assets.logo = g.newPixmap("logo.png", PixmapFormat.ARGB4444);
        Assets.mainMenu = g.newPixmap("mainmenu.png", PixmapFormat.ARGB4444);
        Assets.credits = g.newPixmap("credits.png", PixmapFormat.ARGB4444);
        Assets.buttons = g.newPixmap("buttons.png", PixmapFormat.ARGB4444);
        Assets.numbers = g.newPixmap("numbers.png", PixmapFormat.ARGB4444);
        Assets.ready = g.newPixmap("ready.png", PixmapFormat.ARGB4444);
        Assets.pause = g.newPixmap("pausemenu.png", PixmapFormat.ARGB4444);
        Assets.gameOver = g.newPixmap("gameover.png", PixmapFormat.ARGB4444);
        Assets.piece_1 = g.newPixmap("piece_1.png", PixmapFormat.RGB565);
        Assets.piece_2 = g.newPixmap("piece_2.png", PixmapFormat.RGB565);
        Assets.piece_3 = g.newPixmap("piece_3.png", PixmapFormat.RGB565);
        Assets.piece_4 = g.newPixmap("piece_4.png", PixmapFormat.RGB565);
        Assets.piece_5 = g.newPixmap("piece_5.png", PixmapFormat.RGB565);
        Assets.piece_6 = g.newPixmap("piece_6.png", PixmapFormat.RGB565);
        Assets.piece_7 = g.newPixmap("piece_7.png", PixmapFormat.RGB565);





        Assets.click = game.getAudio().newSound("click.ogg");
        Assets.HardDrop = game.getAudio().newSound("HardDrop.ogg");
        Assets.PieceMoveLR = game.getAudio().newSound("PieceMoveLR.ogg");
        Assets.PieceRotateLR = game.getAudio().newSound("PieceRotateLR.ogg");
        Assets.Theme = game.getAudio().newMusic("Theme.ogg");
        Assets.SoftDrop = game.getAudio().newSound("SoftDrop.ogg");
        Assets.LineClear = game.getAudio().newSound("LineClear.ogg");
        Assets.GameOver = game.getAudio().newSound("GameOver.ogg");
        Assets.PLD = game.getAudio().newSound("PieceLockDown.ogg");
        Assets.Land = game.getAudio().newSound("Land.ogg");


        Settings.load(game.getFileIO());

        game.setScreen(new MainMenuScreen(game));

    }
    
    public void render(float deltaTime) {

    }

    public void pause() {

    }

    public void resume() {

    }

    public void dispose() {

    }
}
