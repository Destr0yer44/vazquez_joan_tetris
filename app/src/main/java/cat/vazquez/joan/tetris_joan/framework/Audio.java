package cat.vazquez.joan.tetris_joan.framework;

public interface Audio {
    public Music newMusic(String filename);

    public Sound newSound(String filename);
}
