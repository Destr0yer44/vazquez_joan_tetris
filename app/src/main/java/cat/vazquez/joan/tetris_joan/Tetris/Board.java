package cat.vazquez.joan.tetris_joan.Tetris;

import android.os.Debug;

import java.util.Random;

import cat.vazquez.joan.tetris_joan.framework.Graphics;
import cat.vazquez.joan.tetris_joan.framework.Pixmap;

/**
 * Created by damviod on 11/12/15.
 */
    public class Board {
    public static final int WORLD_WIDTH = 10;
    public static final int WORLD_HEIGHT = 20;
    static final int SCORE_INCREMENT = 10;

    static final float TICK_FastDown = 0.05f;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.005f;

    public Piece currentPiece;


    public boolean gameOver = false;
    public int score = 0;
    int mapa[][] = new int[WORLD_WIDTH][WORLD_HEIGHT];
    boolean FastDown = false;
    boolean Choca = false;
    boolean Stop = false;
    boolean Puntos = false;
    boolean loop = true;

    float tickTime = 0;
    float previousTick;
    float tick = TICK_INITIAL;

    Random random = new Random();

    int x = 5;
    int y = 1;

    public Board() {
        loop = true;
        currentPiece = new Piece(x - 1, y, random.nextInt(7) + 1);

    }


    public void Render(Graphics g) {  //Un switch per Renderitzar totes les diferents peçes, nomes canvia el color.

        int posx, posy;
        Pixmap piecePixmap = null;

        for (int i = 0; i < WORLD_WIDTH; i++) {
            for (int j = 0; j < WORLD_HEIGHT; j++) {


                switch (mapa[i][j]) {

                    case 1:
                        piecePixmap = Assets.piece_1;

                        posx = i * 32;
                        posy = (j-4) * 32;
                        g.drawPixmap(piecePixmap, posx, posy);
                        break;

                    case 2:
                        piecePixmap = Assets.piece_2;
                        posx = i * 32;
                        posy = (j-4) * 32;
                        g.drawPixmap(piecePixmap, posx, posy);
                        break;

                    case 3:
                        piecePixmap = Assets.piece_3;
                        posx = i * 32;
                        posy = (j-4) * 32;
                        g.drawPixmap(piecePixmap, posx, posy);
                        break;

                    case 4:
                        piecePixmap = Assets.piece_4;
                        posx = i * 32;
                        posy = (j-4) * 32;
                        g.drawPixmap(piecePixmap, posx, posy);
                        break;

                    case 5:
                        piecePixmap = Assets.piece_5;
                        posx = i * 32;
                        posy = (j-4) * 32;
                        g.drawPixmap(piecePixmap, posx, posy);
                        break;

                    case 6:
                        piecePixmap = Assets.piece_6;
                        posx = i * 32;
                        posy = (j-4) * 32;
                        g.drawPixmap(piecePixmap, posx, posy);
                        break;

                    case 7:
                        piecePixmap = Assets.piece_7;
                        posx = i * 32;
                        posy = (j-4) * 32;
                        g.drawPixmap(piecePixmap, posx, posy);
                        break;
                }
            }
        }

        for (int i = 0; i < currentPiece.shape.length; i++) {
            for (int j = 0; j < currentPiece.shape[0].length; j++) {

                if (currentPiece.y + j >= 0 && currentPiece.shape[i][j]) {
                    switch (currentPiece.type) {

                        case 1:
                            piecePixmap = Assets.piece_1;

                            posx = (currentPiece.x + i) * 32;
                            posy = (currentPiece.y + j - 4) * 32;
                            g.drawPixmap(piecePixmap, posx, posy);
                            break;

                        case 2:
                            piecePixmap = Assets.piece_2;
                            posx = (currentPiece.x + i) * 32;
                            posy = (currentPiece.y + j - 4) * 32;
                            g.drawPixmap(piecePixmap, posx, posy);
                            break;

                        case 3:
                            piecePixmap = Assets.piece_3;
                            posx = (currentPiece.x + i) * 32;
                            posy = (currentPiece.y + j - 4) * 32;
                            g.drawPixmap(piecePixmap, posx, posy);
                            break;

                        case 4:
                            piecePixmap = Assets.piece_4;
                            posx = (currentPiece.x + i) * 32;
                            posy = (currentPiece.y + j - 4) * 32;
                            g.drawPixmap(piecePixmap, posx, posy);
                            break;

                        case 5:
                            piecePixmap = Assets.piece_5;
                            posx = (currentPiece.x + i) * 32;
                            posy = (currentPiece.y + j - 4) * 32;
                            g.drawPixmap(piecePixmap, posx, posy);
                            break;

                        case 6:
                            piecePixmap = Assets.piece_6;
                            posx = (currentPiece.x + i) * 32;
                            posy = (currentPiece.y + j - 4) * 32;
                            g.drawPixmap(piecePixmap, posx, posy);
                            break;

                        case 7:
                            piecePixmap = Assets.piece_7;
                            posx = (currentPiece.x + i) * 32;
                            posy = (currentPiece.y + j - 4) * 32;
                            g.drawPixmap(piecePixmap, posx, posy);
                            break;
                    }
                }
            }
        }
    }

    public void MovePieceToRight() {   //Funció per moure les peçes aquesta es cridada en la GameScreen quan llisquem el dit cap a la Dreta.
        if(!isOutOfBounds(currentPiece)){
            if ((currentPiece.x + currentPiece.shape.length) < WORLD_WIDTH && !ComprovarPiezaRight()) {
                currentPiece.x++;
                Assets.PieceMoveLR.play(1);
            }
        }
    }

    public void MovePieceToLeft() { //Funció per moure les peçes aquesta es cridada en la GameScreen quan llisquem el dit cap a l' Esquerra.
        if(!isOutOfBounds(currentPiece)) {
            if ((currentPiece.x - 1) >= 0 && !ComprovarPiezaLeft()) {
                currentPiece.x--;
                Assets.PieceMoveLR.play(1);
            }
        }
    }
    boolean Comprovar()  //Funció que s'encarrega de mirar la seguent posicio per la part de sota de la peça per a frenar-la en cas necesari,
    {
        if (currentPiece.y + currentPiece.shape[0].length == WORLD_HEIGHT - 1) { //Si arriba al terra
            Assets.Land.play(1);
            return true;
        }

        for (int i = currentPiece.shape.length - 1; i >= 0; i--) {
            for (int j = currentPiece.shape[0].length - 1; j >= 0; j--) {
                if (j + currentPiece.y + 1 >= 0) {

                    if (currentPiece.shape[i][j] && mapa[i + currentPiece.x][j + currentPiece.y + 1] != 0) {  //Si choca contra una altre peça.
                        Assets.Land.play(1);
                        return true;
                    }
                        Stop = true;
                }
            }
        }
        return false;
    }

   boolean ComprovarPiezaRight() {  //Funció que serveix per comprovar colisions per la part Esquerra de la peça.

        for (int i = currentPiece.shape.length - 1; i >= 0; i--) {
            for (int j = currentPiece.shape[0].length - 1; j >= 0; j--) {

                if (currentPiece.shape[i][j] && mapa[i + currentPiece.x + 1][j + currentPiece.y] != 0){
                    return true;
                }
            }
        }
        return false;
   }

   boolean ComprovarPiezaLeft() { //Funció que serveix per comprovar colisions per la part Dreta de la peça.

        for (int i = 0; i < currentPiece.shape.length; i++) {
            for (int j = currentPiece.shape[0].length - 1; j >= 0; j--) {
                if (currentPiece.shape[i][j] && mapa[i + currentPiece.x - 1][j + currentPiece.y] != 0){
                    return true;
                }
            }
        }
        return false;
   }

    boolean ComprovarPieza(Piece p) //Función boleana que utilizaremos para hacer comprovaciones de rotaciones en la funcción Rotate().
    {
        for (int i = 0; i < p.shape.length; i++) {
            for (int j = p.shape[0].length - 1; j >= 0; j--) {
                if ((currentPiece.x + p.shape.length > WORLD_WIDTH))
                {
                    if (p.shape.length == 1 || p.shape[0].length == 1)
                        p.x -=3;
                    else p.x--;
                }
                if ((p.shape[i][j] && mapa[i + p.x][j + p.y] != 0) || (currentPiece.y + currentPiece.shape[0].length >= WORLD_HEIGHT)) {
                    return true;
                }
            }
        }
        return false;
    }

   boolean isOutOfBounds(Piece p) //Funció que se utilizar para comprovar si la pieza rotada saldrá de nuestro mapa si es así devolvera true sino false.
   {
       if (p.y < 0 || p.y + p.shape[0].length > WORLD_HEIGHT || p.x < 0 || p.x + p.shape.length > WORLD_WIDTH)
            return true;
       else return false;
   }

    public void Rotate() {
        if (!isOutOfBounds(currentPiece)) {   //Es comprova que la peça estigui situada dins del mapa.
            final int M = currentPiece.shape.length;
            final int N = currentPiece.shape[0].length;
            boolean[][] newShape = new boolean[N][M];
            for (int i = 0; i < M; i++) {
                for (int j = 0; j < N; j++) {
                    newShape[N - j - 1][i] = currentPiece.shape[i][j];
                }
            }

            if (M == 1) {  //Utiliztem aixó per a fer les rotacions de la peça amb shape [4][1]
                currentPiece.x-=2;
                currentPiece.y+=1;
            }
            if (N == 1){  //Utiliztem aixó per a fer les rotacions de la peça amb shape [4][1]
                currentPiece.x+=2;
                currentPiece.y-=1;
            }

            Piece p = new Piece(currentPiece); //Fem un clon de la peça que tenim en cuestió.
            p.shape = newShape;

            if ((currentPiece.x + newShape.length <= WORLD_WIDTH) && (currentPiece.y + newShape[0].length <= WORLD_HEIGHT)) { //Si la nova peça amb la nova shape es a dir la peá rotada es trobará dins del mapa la rotem
                if(!ComprovarPieza(p)) {                                                                                       //  y li apliquem aquesta shape a la nostre peça real.
                    Assets.PieceRotateLR.play(1);
                    currentPiece.shape = newShape;
                }
            } else if ((currentPiece.x + newShape.length > WORLD_WIDTH)) { //Si la peça [4][1] es surt del mapa un cop feta la rotació la desplaçem una miqueta cap a l'esquerra.

                if(!ComprovarPieza(p)){
                    if (M == 1 || N == 1)
                        currentPiece.x -= 2;
                    else
                        currentPiece.x--;

                    Assets.PieceRotateLR.play(1);
                    currentPiece.shape = newShape;
                }
            }
            if (currentPiece.x < 0) { //Si la peça [4][1] se surt del mapa per la part esquerra.
                if (M == 1 || N == 1){
                    currentPiece.x -= currentPiece.x;
                }
                else{
                    currentPiece.x++;
                }
            }
        }
    }

    void Line()
    {
        int[][] mapaInvertido = new int[WORLD_HEIGHT][WORLD_WIDTH];   //Creem un nou mapa per recorrer el board de manera horitzontal.
        for (int i = 0; i < mapa.length; i++) {
            for (int j = 0; j < mapa[0].length; j++) {
                mapaInvertido[j][i] = mapa[i][j];
            }
        }

        int linea;
        int contLineas = 0;

        for (int i = 0; i < mapaInvertido.length; i++) {
            linea = 0;
            for (int j = 0; j < mapaInvertido[0].length; j++) {  //Comprovem totes les lineas hortizontalment per a veure si alguna esta plena.
                if (mapaInvertido[i][j] != 0)
                    linea++;
            }

            if (linea == WORLD_WIDTH)
            {
                for (int l = i; l > 0; l--) {
                    for (int k = 0; k < mapaInvertido[0].length; k++) {  //Si el pas anterior en surt que la linea en cuestió es == WORLD_WIDTH vol dir que aquella linea ha de ser eliminada.
                        mapaInvertido[l][k] = mapaInvertido[l-1][k];

                    }
                }
                contLineas++;
                score += SCORE_INCREMENT * contLineas;
                Assets.LineClear.play(1);
                tick = tick - TICK_DECREMENT;

            }
        }
        for (int i = 0; i < mapaInvertido.length; i++) {
            for (int j = 0; j < mapaInvertido[0].length; j++) {
                mapa[j][i] = mapaInvertido[i][j];
            }
        }

    }

    public void GoDown() {      //Funció per a el moviment desccendent vertical de la peça.

        currentPiece.y++;
        Assets.SoftDrop.play(1);
    }

    public void FastDown() //Funcio que augmenta la velocitat de caida de la peça cuando llisquem cap avall amb els dits.
    {
        if (!FastDown) {
            FastDown = true;
            Stop = false;
            previousTick = tick;
            Assets.HardDrop.play(1);
            tick = TICK_FastDown;
        }

    }


    public void CreatePiece() { //La utilizamos para crear otra pieza cuando la nuestra llega abajo.

        currentPiece = new Piece(x - 1, y, random.nextInt(7) + 1);
    }

    public void update(float deltaTime) {
        if (gameOver) return;
        tickTime += deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;

            if (Comprovar()) {

                if (currentPiece.y <= y) {
                    gameOver = true;
                    return;
                }

                for (int i = 0; i < currentPiece.shape.length; i++) {
                    for (int j = 0; j < currentPiece.shape[0].length; j++) {
                        if (currentPiece.shape[i][j]) {
                            mapa[i + currentPiece.x][(j + currentPiece.y)] = currentPiece.type;
                        }
                    }
                }
                Line();
                CreatePiece();

                if ((currentPiece.y + currentPiece.shape[0].length == WORLD_HEIGHT - 1) || Stop){ //Lo utilizamos para parar la caida Fast, si ha tocado suelo o
                    if (FastDown) {                                                                // Stop es true es decir a chocado contra otra pieza vuelve a su caída normal.
                        FastDown = false;
                        tick = previousTick;
                    }
                }

            } else {
                GoDown();
            }
        }
    }
}

