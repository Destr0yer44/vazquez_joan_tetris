package cat.vazquez.joan.tetris_joan.framework;

import cat.vazquez.joan.tetris_joan.framework.Graphics.PixmapFormat;

public interface Pixmap {
    public int getWidth();

    public int getHeight();

    public PixmapFormat getFormat();

    public void dispose();
}
